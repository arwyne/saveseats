const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const cinemaSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    seatsNo: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Cinema", cinemaSchema);
