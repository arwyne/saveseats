const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const screeningSchema = new Schema(
  {
    screeningDate: {
      type: Date,
      required: true,
    },
    screeningTime: {
      type: String,
      required: true,
    },
    movieId: {
      type: mongoose.ObjectId,
      required: true,
    },
    cinemaId: {
      type: mongoose.ObjectId,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Screening", screeningSchema);
