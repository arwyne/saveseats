const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reservationSchema = new Schema(
  {
    reservationId: {
      type: mongoose.ObjectId,
      required: true,
    },
    screeningId: {
      type: mongoose.ObjectId,
      required: true,
    },
    seatId: {
      type: mongoose.ObjectId,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("ReservedSeat", reservationSchema);
