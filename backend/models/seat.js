const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const seatSchema = new Schema(
  {
    row: {
      type: String,
      required: true,
    },
    number: {
      type: Number,
      required: true,
    },
    available: {
      type: Boolean,
      default: true, // true if available, false if occupied
    },
    cinemaId: {
      type: mongoose.ObjectId,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Seat", seatSchema);
