const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");

/* for graphql playground*/
const graphqlHTTP = require("express-graphql");
const graphqlSchema = require("./gql-schema");

/* Database Connection */
mongoose.connect("mongodb://localhost:27017/saveseats", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

/* Middlewares */
app.use(cors());

/* gql playground */
app.use("/graphql", graphqlHTTP({ schema: graphqlSchema, graphiql: true }));

/* Server Initialization */
app.listen(4000, () => {
  console.log("Now Serving on port 4000");
});
