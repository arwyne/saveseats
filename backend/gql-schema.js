const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLBoolean,
} = require("graphql");

const graphqlIsoDate = require("graphql-iso-date");
const { GraphQLDateTime, GraphQLDate } = graphqlIsoDate;

/* models */
const Cinema = require("./models/cinema");
const Movie = require("./models/movie");
const Reservation = require("./models/reservation");
const ReservedSeat = require("./models/reservedSeat");
const Screening = require("./models/screening");
const Seat = require("./models/seat");
const User = require("./models/user");

/* login */
const bcrypt = require("bcrypt");
const auth = require("./jwt-auth");

/*

  GQL Object Type
  
*/
const CinemaType = new GraphQLObjectType({
  name: "Cinema",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    seatsNo: { type: GraphQLInt },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
  }),
});

const MovieType = new GraphQLObjectType({
  name: "Movie",
  fields: () => ({
    id: { type: GraphQLID },
    title: { type: GraphQLString },
    genre: { type: GraphQLString },
    description: { type: GraphQLString },
    duration: { type: GraphQLInt },
    image: { type: GraphQLString },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
  }),
});

const ReservationType = new GraphQLObjectType({
  name: "Reservation",
  fields: () => ({
    id: { type: GraphQLID },
    referenceNo: { type: GraphQLString },
    quantity: { type: GraphQLInt },
    totalPrice: { type: GraphQLInt },
    userId: { type: GraphQLID },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
    user: {
      type: UserType,
      resolve: (parent, args) => {
        return User.findById(parent.userId);
      },
    },
    reservedSeats: {
      type: new GraphQLList(ReservedSeatType),
      resolve: (parent, args) => {
        return ReservedSeat.find({ reservationId: parent.id });
      },
    },
  }),
});

const ReservedSeatType = new GraphQLObjectType({
  name: "ReservedSeat",
  fields: () => ({
    id: { type: GraphQLID },
    reservationId: { type: GraphQLID },
    screeningId: { type: GraphQLID },
    seatId: { type: GraphQLID },
    reservation: {
      type: ReservationType,
      resolve: (parent, args) => {
        return Reservation.findById(parent.reservationId);
      },
    },
    screening: {
      type: ScreeningType,
      resolve: (parent, args) => {
        return Screening.findById(parent.screeningId);
      },
    },
    seat: {
      type: SeatType,
      resolve: (parent, args) => {
        return Seat.findById(parent.seatId);
      },
    },

    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
  }),
});

const ScreeningType = new GraphQLObjectType({
  name: "Screening",
  fields: () => ({
    id: { type: GraphQLID },
    screeningDate: { type: GraphQLDate },
    screeningTime: { type: GraphQLString },
    movieId: { type: GraphQLID },
    cinemaId: { type: GraphQLID },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
    movie: {
      type: MovieType,
      resolve: (parent, args) => {
        return Movie.findById(parent.movieId);
      },
    },
    cinema: {
      type: CinemaType,
      resolve: (parent, args) => {
        return Cinema.findById(parent.cinemaId);
      },
    },
    reservedSeats: {
      type: new GraphQLList(ReservedSeatType),
      resolve: (parent, args) => {
        return ReservedSeat.find({ screeningId: parent.id });
      },
    },
  }),
});

const SeatType = new GraphQLObjectType({
  name: "Seat",
  fields: () => ({
    id: { type: GraphQLID },
    row: { type: GraphQLString },
    number: { type: GraphQLInt },
    available: { type: GraphQLBoolean }, // true if available, false if occupied
    cinemaId: { type: GraphQLID },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
    cinema: {
      type: CinemaType,
      resolve: (parent, args) => {
        return Cinema.findById(parent.cinemaId);
      },
    },
    reservedSeat: {
      type: ReservedSeatType,
      resolve: (parent, args) => {
        return ReservedSeat.find({ seatId: parent.id });
      },
    },
  }),
});

const UserType = new GraphQLObjectType({
  name: "User",
  fields: () => ({
    id: { type: GraphQLID },
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    email: { type: GraphQLString },
    username: { type: GraphQLString },
    mobileNo: { type: GraphQLString },
    role: { type: GraphQLString },
    token: { type: GraphQLString },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
    reservations: {
      type: new GraphQLList(ReservationType),
      resolve: (parent, args) => {
        return Reservation.find({ userId: parent.id });
      },
    },
  }),
});

/* 

  GQL Root Query 
  
*/

const RootQuery = new GraphQLObjectType({
  name: "Query",
  fields: {
    /* to retrieve all users */
    users: {
      type: new GraphQLList(UserType),
      resolve: (parents, args) => {
        return User.find({});
      },
    },

    /* to retrieve a single user */
    user: {
      type: UserType,
      args: {
        id: { type: GraphQLID },
      },
      resolve: (parent, args) => {
        return User.findById(args.id);
      },
    },

    cinemas: {
      type: new GraphQLList(CinemaType),
      resolve: (parents, args) => {
        return Cinema.find({});
      },
    },

    cinema: {
      type: CinemaType,
      args: {
        id: { type: GraphQLID },
      },
      resolve: (parents, args) => {
        return Cinema.findById(args.id);
      },
    },

    movies: {
      type: new GraphQLList(MovieType),
      resolve: (parents, args) => {
        return Movie.find({});
      },
    },

    movie: {
      type: MovieType,
      args: {
        id: { type: GraphQLID },
      },
      resolve: (parents, args) => {
        return Movie.findById(args.id);
      },
    },

    reservations: {
      type: new GraphQLList(ReservationType),
      resolve: (parents, args) => {
        return Reservation.find({});
      },
    },

    reservation: {
      type: ReservationType,
      args: {
        id: { type: GraphQLID },
      },
      resolve: (parents, args) => {
        return Reservation.findById(args.id);
      },
    },

    reservedSeats: {
      type: new GraphQLList(ReservedSeatType),
      resolve: (parents, args) => {
        return ReservedSeat.find({});
      },
    },

    reservedSeat: {
      type: ReservedSeatType,
      args: {
        id: { type: GraphQLID },
      },
      resolve: (parents, args) => {
        return ReservedSeat.findById(args.id);
      },
    },

    screenings: {
      type: new GraphQLList(ScreeningType),
      resolve: (parents, args) => {
        return Screening.find({});
      },
    },

    screening: {
      type: ScreeningType,
      args: {
        id: { type: GraphQLID },
      },
      resolve: (parents, args) => {
        return Screening.findById(args.id);
      },
    },

    seats: {
      type: new GraphQLList(SeatType),
      resolve: (parents, args) => {
        return Seat.find({});
      },
    },

    seat: {
      type: SeatType,
      args: {
        id: { type: GraphQLID },
      },
      resolve: (parents, args) => {
        return Seat.findById(args.id);
      },
    },
  },
});

/* 

Mutation

*/

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    /* 
        User Mutation
    */
    addUser: {
      type: UserType,
      args: {
        firstName: { type: new GraphQLNonNull(GraphQLString) },
        lastName: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        username: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
        mobileNo: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (parent, args) => {
        let newUser = new User({
          firstName: args.firstName,
          lastName: args.lastName,
          email: args.email,
          username: args.username,
          password: bcrypt.hashSync(args.password, 10),
          mobileNo: args.mobileNo,
          role: "0", // 0 = normal user, 1 = admin
        });
        return newUser.save();
      },
    },
    updateUser: {
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        firstName: { type: new GraphQLNonNull(GraphQLString) },
        lastName: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        username: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
        mobileNo: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (parent, args) => {
        let userId = { _id: args.id };
        let updates = {
          firstName: args.firstName,
          lastName: args.lastName,
          email: args.email,
          username: args.username,
          password: args.password,
          mobileNo: args.mobileNo,
        };
        return User.findOneAndUpdate(userId, updates, (user) => user);
      },
    },
    deleteUser: {
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let userId = { _id: args.id };
        return User.findOneAndDelete(userId);
      },
    },

    /*
        Cinema Mutation
    */
    addCinema: {
      type: CinemaType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        seatsNo: { type: new GraphQLNonNull(GraphQLInt) },
      },
      resolve: (parent, args) => {
        let newCinema = new Cinema({
          name: args.name,
          seatsNo: args.seatsNo,
        });
        return newCinema.save();
      },
    },

    /*
        Movie Mutation
    */
    addMovie: {
      type: MovieType,
      args: {
        title: { type: new GraphQLNonNull(GraphQLString) },
        genre: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: new GraphQLNonNull(GraphQLString) },
        duration: { type: new GraphQLNonNull(GraphQLInt) },
        image: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (parent, args) => {
        let newMovie = new Movie({
          title: args.title,
          genre: args.genre,
          description: args.description,
          duration: args.duration,
          image: args.image,
        });
        return newMovie.save();
      },
    },

    updateMovie: {
      type: MovieType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        title: { type: new GraphQLNonNull(GraphQLString) },
        genre: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: new GraphQLNonNull(GraphQLString) },
        duration: { type: new GraphQLNonNull(GraphQLInt) },
        image: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (parent, args) => {
        let movieId = { _id: args.id };
        let updates = {
          title: args.title,
          genre: args.genre,
          description: args.description,
          duration: args.duration,
          image: args.image,
        };
        return Movie.findOneAndUpdate(movieId, updates, (movie) => movie);
      },
    },

    deleteMovie: {
      type: MovieType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let movieId = { _id: args.id };
        return Movie.findOneAndDelete(movieId);
      },
    },

    /*
        Reservation Mutation
    */
    addReservation: {
      type: ReservationType,
      args: {
        referenceNo: { type: new GraphQLNonNull(GraphQLString) },
        quantity: { type: new GraphQLNonNull(GraphQLInt) },
        totalPrice: { type: new GraphQLNonNull(GraphQLInt) },
        userId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let newReservation = new Reservation({
          referenceNo: args.referenceNo,
          quantity: args.quantity,
          totalPrice: args.totalPrice,
          userId: args.userId,
        });
        return newReservation.save();
      },
    },

    updateReservation: {
      type: ReservationType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        referenceNo: { type: new GraphQLNonNull(GraphQLString) },
        quantity: { type: new GraphQLNonNull(GraphQLInt) },
        totalPrice: { type: new GraphQLNonNull(GraphQLInt) },
        userId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let reservationId = { _id: args.id };
        let updates = {
          referenceNo: args.referenceNo,
          quantity: args.quantity,
          totalPrice: args.totalPrice,
          userId: args.userId,
        };
        return Reservation.findOneAndUpdate(
          reservationId,
          updates,
          (reservation) => reservation
        );
      },
    },

    deleteReservation: {
      type: ReservationType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let reservationId = { _id: args.id };
        return Reservation.findOneAndDelete(reservationId);
      },
    },

    /*
        Reserved Seat Mutation
    */
    addReservedSeat: {
      type: ReservedSeatType,
      args: {
        reservationId: { type: new GraphQLNonNull(GraphQLID) },
        screeningId: { type: new GraphQLNonNull(GraphQLID) },
        seatId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let newReservedSeat = new ReservedSeat({
          reservationId: args.reservationId,
          screeningId: args.screeningId,
          seatId: args.seatId,
        });
        return newReservedSeat.save();
      },
    },

    updateReservedSeat: {
      type: ReservedSeatType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        reservationId: { type: new GraphQLNonNull(GraphQLID) },
        screeningId: { type: new GraphQLNonNull(GraphQLID) },
        seatId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let reservedSeatId = { _id: args.id };
        let updates = {
          reservationId: args.reservationId,
          screeningId: args.screeningId,
          seatId: args.seatId,
        };
        return ReservedSeat.findOneAndUpdate(
          reservedSeatId,
          updates,
          (reservedSeat) => reservedSeat
        );
      },
    },

    deleteReservedSeat: {
      type: ReservedSeatType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let reservedSeatId = { _id: args.id };
        return ReservedSeat.findOneAndDelete(reservedSeatId);
      },
    },

    /*
        Screening Mutation
    */
    addScreening: {
      type: ScreeningType,
      args: {
        screeningDate: { type: new GraphQLNonNull(GraphQLDate) },
        screeningTime: { type: new GraphQLNonNull(GraphQLString) },
        movieId: { type: new GraphQLNonNull(GraphQLID) },
        cinemaId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let newScreening = new Screening({
          screeningDate: args.screeningDate,
          screeningTime: args.screeningTime,
          movieId: args.movieId,
          cinemaId: args.cinemaId,
        });
        return newScreening.save();
      },
    },

    updateScreening: {
      type: ScreeningType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        screeningDate: { type: new GraphQLNonNull(GraphQLDate) },
        screeningTime: { type: new GraphQLNonNull(GraphQLString) },
        movieId: { type: new GraphQLNonNull(GraphQLID) },
        cinemaId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let screeningId = { _id: args.id };
        let updates = {
          screeningDate: args.screeningDate,
          screeningTime: args.screeningTime,
          movieId: args.movieId,
          cinemaId: args.cinemaId,
        };
        return Screening.findOneAndUpdate(
          screeningId,
          updates,
          (screening) => screening
        );
      },
    },

    deleteScreening: {
      type: ScreeningType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let screeningId = { _id: args.id };
        return Screening.findOneAndDelete(screeningId);
      },
    },
    /*  
        Seat Mutation
    */

    addSeat: {
      type: SeatType,
      args: {
        row: { type: new GraphQLNonNull(GraphQLString) },
        number: { type: new GraphQLNonNull(GraphQLInt) },
        cinemaId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (parent, args) => {
        let newSeat = new Seat({
          row: args.row,
          number: args.number,
          cinemaId: args.cinemaId,
        });
        return newSeat.save();
      },
    },

    login: {
      type: UserType,
      args: {
        email: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (parent, args) => {
        let query = User.findOne({ email: args.email });

        return query
          .then((user) => user)
          .then((user) => {
            if (user == null) {
              return null;
            }

            let isPasswordMatched = bcrypt.compareSync(
              args.password,
              user.password
            );

            if (isPasswordMatched) {
              user.token = auth.createToken(user.toObject());
              return user;
            } else {
              return null;
            }
          });
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
