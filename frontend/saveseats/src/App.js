import React from "react";
import "./App.css";

/* Page Imports */
import RegisterPage from "./pages/Register/RegisterPage";
import LoginPage from "./pages/Login/LoginPage";

/* for router */
import { BrowserRouter, Route, Switch } from "react-router-dom";

/* for apollo client */
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

// Apollo Setup
const client = new ApolloClient({ uri: "http://localhost:4000/graphql" });

const App = () => {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <Switch>
          <RegisterPage exact path="/register" component={RegisterPage} />
          <LoginPage exact path="/login" component={LoginPage} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
};

export default App;
