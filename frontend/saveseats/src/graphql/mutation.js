import { gql } from "apollo-boost";

const addUserMutation = gql`
  mutation(
    $firstName: String!
    $lastName: String!
    $email: String!
    $username: String!
    $password: String!
    $mobileNo: String!
  ) {
    addUser(
      firstName: $firstName
      lastName: $lastName
      email: $email
      username: $username
      password: $password
      mobileNo: $mobileNo
    ) {
      firstName
      lastName
      email
      username
      mobileNo
    }
  }
`;

const loginMutation = gql`
  mutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      firstName
      role
      token
    }
  }
`;

export { addUserMutation, loginMutation };
